#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {}
  Serial.println("PC");

  mySerial.begin(9600);
  while (!mySerial) {}
  mySerial.println("chat");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (mySerial.available()){
    Serial.write(mySerial.read());
  }
  if (Serial.available()){
    mySerial.write(Serial.read());
  }
}
