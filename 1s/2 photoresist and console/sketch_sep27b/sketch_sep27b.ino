/*
#define analog_pin A0
#define analog_pin A1
#define analog_pin A2
#define analog_pin A3
#define analog_pin A4
#define analog_pin A5
*/
static const uint8_t analog_pins[] = {A0, A1, A2,A3,A4,A5};
int pin;
int code;
int cnt;
int i;
int j;
bool flg = false;

String hash_find(String data){
  int lg = data.length();
  int data_int[lg];
  char data_char[lg];
  data.toCharArray(data_char, lg+1);
  int ln = 6;
  int num = 0;
  String result = "";
  for (i=0; i<lg; i++) {
    data_int[i]=int(data_char[i]);
  }
  for (i=0; i<ln+1; i++) {
    num = 0;
    for (j=0; j<lg; j++) {
      num += (data_int[j] >> (ln - i)) % 2;
    }
    num %= 2;
    result += String(num);
  }
  return result;
}

void check_serial() {
  while(Serial.available() > 0){
    String message = Serial.readString();
    code = message.toInt();
    //вернуть значение аналогового пина
    if (code == 1) {
      while (Serial.available() == 0) {}
      pin = Serial.readString().toInt();
      Serial.print(analogRead(analog_pins[pin]));
    }
    //вернуть зачения всех аналаговых пинов
    else if (code == 2) {
      for (int i = 0; i <= 5; i++) {
        Serial.print(analogRead(analog_pins[i]));
      }
    }
    //вернуть значение цифрового пина
    else if (code == 3){
      while (Serial.available() == 0) {} 
      pin = Serial.readString().toInt();
      Serial.print(digitalRead(pin));
    }
      //вернуть значения всех цифровых пинов
    else if (code == 4){
      for (i = 0; i < 14; i++) {
        Serial.print(digitalRead(i));
      }
    }
      //изменить значение цифрового пина X на Y
    else if (code == 5){
      while (Serial.available() == 0) {}
      pin = Serial.readString().toInt();
      while (Serial.available() == 0) {}
      digitalWrite(pin, Serial.readString().toInt());
    }
      //изменить значение цифровых 2-12 пинов на Y
    else if (code == 6){
      while (Serial.available() == 0) {} 
      pin = Serial.readString().toInt();
      for (i = 2; i < 13; i++) {
        digitalWrite(i, pin); 
      }
    }
      //изменить значение цифровых пинов X1, ..., Xn на Y1, ..., Yn
    else if (code == 7){
      while (Serial.available() == 0) {} 
      cnt = Serial.readString().toInt();
      for (i = 0; i < cnt; i++) {
        while (Serial.available() == 0) {} 
        pin = Serial.readString().toInt();
        while (Serial.available() == 0) {} 
        digitalWrite(pin, Serial.readString().toInt()); 
      }
    }
    else if (code == 8){
      for (i = 1; i < 8; i++) {
        Serial.print("check" + String(i));
      }
    }    
      //вкл/выкл тестовой отправки данных в буфер
    else if (code == 9){
      if (flg) {
        flg = false;
      } else {
        flg = true;
      }
    }
    /*
    switch(code) {
      //вернуть значение аналогового пина
      //case 1:
        while (Serial.available() == 0) {}
        int pin = Serial.readString().toInt();
        Serial.println(analogRead(analog_pins[pin]));
        //Serial.println("case 1");
      break;
      //вернуть зачения всех аналаговых пинов
      case 2:
        for (int i = 0; i <= 5; i++) {
          Serial.println(analogRead(analog_pins[i]));
        }
        //Serial.println("case 2");
      break;
      //вернуть значение цифрового пина
      case 3:
        while (Serial.available() == 0) {} 
        pin = Serial.readString().toInt();
        Serial.println(digitalRead(pin));
        //Serial.println("case 3");
      break;
      //вернуть зачения всех цифровых пинов
      case 4:
        for (int i = 2; i <= 13; i++) {
          Serial.println(digitalRead(i));
        }
        //Serial.println("case 4");
      break;
      //изменить значение цифрового пина X на Y
      case 5:
        while (Serial.available() == 0) {}
        pin = Serial.readString().toInt();
        while (Serial.available() == 0) {}
        digitalWrite(pin, Serial.readString().toInt()); 
        //Serial.println("case 5");
      break;
      //изменить значение цифровых пинов на Y
      case 6:
        while (Serial.available() == 0) {} 
        int mode = Serial.readString().toInt();
        for (int i = 2; i < 14; i++) {
          digitalWrite(i, mode); 
        }
        //Serial.println("case 6");
      break;
      //изменить значение цифровых пинов X1, ..., Xn на Y1, ..., Yn
      case 7:
        while (Serial.available() == 0) {} 
        int cnt = Serial.readString().toInt();
        for (int i = 0; i < cnt; i++) {
          while (Serial.available() == 0) {} 
          pin = Serial.readString().toInt();
          while (Serial.available() == 0) {} 
          digitalWrite(pin, Serial.readString().toInt()); 
        }
        //Serial.println("case 7");
      break;
      //вкл/выкл тестовой отправки данных в буфер
      case 9:
        Serial.println("lol");
        if (flg) {
          flg = false;
        } else {
          flg = true;
        }
      break;
    }
    */
  }
}

void setup(){
  Serial.begin(9600);
  while (!Serial){
    
  }
  //pinMode(led_pin, OUTPUT);
}

void loop(){
  /*
  int raw_val = analogRead(analog_pin);
  int val = map(raw_val, 0, 1000, 15, 1)*100;
  Serial.println(val);
  digitalWrite(led_pin, HIGH);
  delay(val);
  //Serial.println("Led is on");
  digitalWrite(led_pin, LOW);
  delay(val);
  //Serial.println("Led is off");
  */
  check_serial();
  if (flg) {
    Serial.print("check");
  }
}