#define minapin1 9
#define plusapin1 10
#define plusapin2 11
#define minapin2 12
String command;
char chr;
int mode;
int spd;


void setup() {
  Serial.begin(9600);
  while (!Serial) {}
  pinMode(plusapin1, OUTPUT);
  pinMode(plusapin2, OUTPUT);
  pinMode(minapin1, OUTPUT);
  pinMode(minapin2, OUTPUT);
  digitalWrite(plusapin1, LOW);
  digitalWrite(plusapin2, LOW);
  digitalWrite(minapin1, HIGH);
  digitalWrite(minapin2, HIGH);
  Serial.print("Clockwise(1), counter-clockwise(2) or turn off(0)?: ");
}


void loop(){
  if (Serial.available() > 0){
    delay(100);
    command = Serial.readString();
    command.trim();
    Serial.println(command);
    if (command == "0"){
      digitalWrite(minapin1, HIGH);
      digitalWrite(minapin2, HIGH);
      digitalWrite(plusapin1, LOW);
      digitalWrite(plusapin2, LOW);
    } else if ((command == "2") or (command == "1")){
      spd = 0;
      mode = command.toInt();
      Serial.print("Speed (0-255)?: ");
      while (Serial.available() == 0);
      command = Serial.readString();
      Serial.println(command);
      for (byte i = 0; i <= command.length(); i++) {
        chr = command[i];
        if (isDigit(chr)) {
          spd = spd*10 + (chr-'0');
        }
      }
      if ((spd > -1) and (spd < 256)) {
        if (mode == 1) {
          digitalWrite(minapin1, HIGH);
          digitalWrite(minapin2, LOW);
          digitalWrite(plusapin1, LOW);
          analogWrite(plusapin2, spd);
        } else {
          digitalWrite(minapin1, LOW);
          digitalWrite(minapin2, HIGH);
          analogWrite(plusapin1, spd);
          digitalWrite(plusapin2, LOW);
        }
      } else {
        Serial.println("wrong speed");
      }
    } else {
      Serial.println("wrong command");
    }
    Serial.print("Clockwise(1), counter-clockwise(2) or turn off(0)?: ");
  }
}
