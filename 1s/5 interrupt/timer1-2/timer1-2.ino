#define led_pin1 3
bool mode = false;
bool old_mode = false;
long mls = 0;

void setup(){
  Serial.begin(9600);
  pinMode(led_pin1, OUTPUT);

  cli();
  //1
  TCCR2A = 0;
  TCCR2B = 0;
  TCCR2B |= (1 << CS10);
  TCCR2B |= (1 << CS12);
  TIMSK2 = (1 << TOIE1);

  //2
  TCCR1A = 0;
  TCCR1B = 0;
  OCR1A = 16000;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  TIMSK1 |= (1 << OCIE1A);
  sei();
}

//1
ISR(TIMER2_OVF_vect){
  mode = !mode;
}

//2
long millis2(){
  return mls;
}
ISR(TIMER1_COMPA_vect){
  mls++;
}

void loop(){
  //1
  digitalWrite(led_pin1, mode);
  if (mode != old_mode){
    old_mode = mode;
  }

  //2
  /*
  int a = int(millis());
  int b = int(millis2());
  Serial.print(a);
  Serial.print(' ');
  Serial.print(b);
  Serial.print(' ');
  */
  if (millis() == millis2()) {
  	Serial.println("True");
  } else {
    Serial.println("False");
  }
}