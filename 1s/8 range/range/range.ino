#define dpin_trig 12
#define dpin_echo 11
#define apin_sens A0


float echo_get_dist(){
  digitalWrite(dpin_trig, LOW);
  delayMicroseconds(5);
  digitalWrite(dpin_trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(dpin_trig, LOW);
  float dist = pulseIn(dpin_echo, HIGH);
  dist = (dist / 2) / 29;
  return dist;
}

float depend = 0;
float ik_get_dist(float k = 12.3){
  int16_t val  = analogRead(apin_sens);
  depend = pow(val * 0.005, -1.1);
  float dist = k * depend;
  return dist;
}


void setup(){
  Serial.begin (9600);
  while (!Serial) {}

  pinMode(dpin_trig, OUTPUT);
  pinMode(dpin_echo, INPUT);
  pinMode(apin_sens, INPUT);

  float k = 1;
  float d1 = echo_get_dist();
  float d2 = ik_get_dist(k);
  bool chk = true;
  int i = 0;
  while (i < 500){
    do {
      d1 = echo_get_dist();
      d2 = ik_get_dist(k);
    } while (d1 > 80);
    
    if (chk){
      chk = false;
      k = d1/depend;
    } else {
      k = (k + d1/depend) / 2;
    }

    Serial.println("d1: " + String(d1));
    Serial.println("d2: " + String(d2));
    Serial.println("K: " + String(k));
    Serial.println();
    delay(50);
    i++;
  }
  delay(500);
}

void loop(){
  float echo_dist = echo_get_dist();
  float ik_dist = ik_get_dist();
  Serial.println("Эхо: " + String(echo_dist) + " см.");
  Serial.println("IK:  " + String(ik_dist) + " см.");
  Serial.println();
  delay(750);
}
