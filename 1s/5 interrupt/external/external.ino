#define led_pin 3
#define btn_pin 7
bool mode = false;

void setup() {
  Serial.begin(9600);
  //attachInterrupt(0, blink, RISING);
  //pinMode(btn_pin, INPUT);
  pinMode(led_pin, OUTPUT);
  
  cli();
  EICRA = 0;
  EICRA |= 1 << ISC01;
  EICRA |= 1 << ISC00;
  EIMSK |= (1<<INT0);
  sei();
}

ISR(INT0_vect){ 
  mode = !mode;
}

/*
void blink() {
  Serial.println("check");
  digitalWrite(led_pin, digitalRead(btn_pin));
  //digitalWrite(led_pin, !digitalRead(led_pin));
  //delay(10);
}
*/

void loop() {
  digitalWrite(led_pin, mode);
}
