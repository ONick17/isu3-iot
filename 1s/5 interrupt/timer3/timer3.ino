#define led_pin31 4
#define led_pin32 5
#define led_pin33 6
bool mode1 = false;
bool mode2 = false;
bool mode3 = false;
bool keke2 = false;
bool keke3 = false;

void setup(){
  Serial.begin(9600);
  pinMode(led_pin31, OUTPUT);
  pinMode(led_pin32, OUTPUT);
  pinMode(led_pin33, OUTPUT);

  cli();
  //3
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 62500;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS12);
  TIMSK1 |= (1 << OCIE1A);
  sei();
}

//3
ISR(TIMER1_COMPA_vect){
  mode1 = !mode1;
  if (keke2) {
    mode2 = !mode2;
  } else {
    mode2 = false;
  }
  if (keke3) {
    mode3 = !mode3;
  } else {
    mode3 = false;
  }
}

void loop(){
  //3
  digitalWrite(led_pin31, mode1);
  digitalWrite(led_pin32, mode2);
  digitalWrite(led_pin33, mode3);
  if (Serial.available() >= 1){
    String inp = Serial.readString();
    if (inp == "2"){
      keke2 = !keke2;
    }
    else if (inp == "3"){
      keke3 = !keke3;
    }
  }
}