#define lwheeld 4
#define lwheels 5
#define rwheels 6
#define rwheeld 7
#define trig 12
#define echo 13
#define vcc 11
#define sharp A0


bool go = false;
bool turnL = false;
bool turnR = false;
bool rotL = false;
bool rotR = false;

int changed1 = 1;
int changed2 = 0;

float distL = 0;
float distLO = 0;
float distF = 0;
float minL = 10;
float maxL = 12;
float minF = 15;
float no_left = 5;

int spd = 200;
int turn_spd = int(200/2);
int rot_spd = 150;
int timer;


void setup() {
  Serial.begin(9600);
  pinMode(lwheeld, OUTPUT);
  pinMode(lwheels, OUTPUT);
  pinMode(rwheeld, OUTPUT);
  pinMode(rwheels, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(vcc, OUTPUT);
  pinMode(sharp, INPUT);
  digitalWrite(vcc, HIGH);

  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  distL = (pulseIn(echo, HIGH) / 2) / 29.1;
}


void loop() {
  distF = 4165.4686 / (analogRead(sharp) - 67.8245);
  
  distLO = distL;
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  distL = (pulseIn(echo, HIGH) / 2) / 29.1;
  
  if (changed1 != changed2) {
    Serial.print("distF: ");
    Serial.println(distF);
    Serial.print("distL: ");
    Serial.println(distL);
    Serial.print("distLO: ");
    Serial.println(distLO);
    Serial.print("go: ");
    Serial.println(String(go));
    Serial.print("turnL: ");
    Serial.println(String(turnL));
    Serial.print("turnR: ");
    Serial.println(String(turnR));
    Serial.print("rotL: ");
    Serial.println(String(rotL));
    Serial.print("rotR: ");
    Serial.println(String(rotR));
    Serial.println();
    changed2 = changed1;
  }
  
  update_states();
  do_move();
  delay(200);
}


void update_states(){
  if (distL - distLO > no_left) {
    go = false;
    turnR = false;
    turnL = false;
    rotR = false;
    rotL = true;
    changed1 = 2;
  } else if ((distF < minF) or (distL < minL/2)) {
    go = false;
    turnR = false;
    turnL = false;
    rotR = true;
    rotL = false;
    changed1 = 1;
  } else {
    rotL = false;
    rotR = false;
    if (distL < minL) {
      turnR = true;
      turnL = false;
      go = false;
      changed1 = 3;
    } else if (distL > maxL) {
      turnR = false;
      turnL = true;
      go = false;
      changed1 = 4;
    } else {
      turnR = false;
      turnL = false;
      go = true;
      changed1 = 5;
    }
  }
}


void do_move(){
  if (go) {
    digitalWrite(rwheeld, HIGH);
    analogWrite(rwheels, spd);
    digitalWrite(lwheeld, LOW);
    analogWrite(lwheels, spd);
  } else if (turnR) {
    analogWrite(rwheels, spd);
    digitalWrite(rwheeld, HIGH);
    analogWrite(lwheels, turn_spd);
    digitalWrite(lwheeld, LOW);
  } else if (turnL) {
    analogWrite(rwheels, turn_spd);
    digitalWrite(rwheeld, HIGH);
    analogWrite(lwheels, spd);
    digitalWrite(lwheeld, LOW);
  } else if (rotR) {
    analogWrite(rwheels, rot_spd);
    digitalWrite(rwheeld, HIGH);
    analogWrite(lwheels, rot_spd);
    digitalWrite(lwheeld, HIGH);
  } else if (rotL) {
    analogWrite(rwheels, rot_spd);
    digitalWrite(rwheeld, LOW);
    analogWrite(lwheels, rot_spd);
    digitalWrite(lwheeld, LOW);
  } else {
    analogWrite(rwheels, 0);
    analogWrite(lwheels, 0);
  }
}
