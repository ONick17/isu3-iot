#define ledpin1 2
#define ledpin2 3
#define ledpin3 4
#define ledpin4 5
#define minpin1 6
#define minpin2 7
bool l16 = false;
bool l17 = false;
bool l18 = false;
bool l125 = false;
bool l126 = false;
bool l127 = false;
bool l128 = false;
bool l135 = false;
bool l136 = false;
bool l137 = false;
bool l138 = false;
bool l145 = false;
bool l146 = false;
bool l147 = false;
bool l148 = false;
bool l167 = false;
bool l168 = false;
bool l178 = false;


void false_all(){
    l16 = false;
    l17 = false;
    l18 = false;
    l125 = false;
    l126 = false;
    l127 = false;
    l128 = false;
    l135 = false;
    l136 = false;
    l137 = false;
    l138 = false;
    l145 = false;
    l146 = false;
    l147 = false;
    l148 = false;
    l167 = false;
    l168 = false;
    l178 = false;
  }

void setup (){
  Serial.begin(9600);
  pinMode(ledpin1, OUTPUT);
  pinMode(ledpin2, OUTPUT);
  pinMode(ledpin3, OUTPUT);
  pinMode(ledpin4, OUTPUT);
  pinMode(minpin1, OUTPUT);
  pinMode(minpin2, OUTPUT);
  digitalWrite(ledpin1, LOW);
  digitalWrite(ledpin2, LOW);
  digitalWrite(ledpin3, LOW);
  digitalWrite(ledpin4, LOW);
  digitalWrite(minpin1, HIGH);
  digitalWrite(minpin2, HIGH);
}

void loop(){
  if (Serial.available() > 0){
    delay(100);
    false_all();
    String command = Serial.readString();
    command.trim();
    //command.replace("\n","");
    //Serial.println(command);
    turn_off();
    if (command == "1"){
      Serial.println("1");
      led1_on();
    }
    else if (command == "2"){
      Serial.println("2");
      led2_on();
    }
    else if (command == "3"){
      Serial.println("3");
      led3_on();
    }
    else if (command == "4"){
      Serial.println("4");
      led4_on();
    }
    else if (command == "5"){
      Serial.println("5");
      led5_on();
    }
    else if (command == "6"){
      Serial.println("6");
      led6_on();
    }
    else if (command == "7"){
      Serial.println("7");
      led7_on();
    }
    else if (command == "8"){
      Serial.println("8");
      led8_on();
    }
    else if (command == "15"){
      Serial.println("15");
      led15_on();
    }
    else if (command == "26"){
      Serial.println("26");
      led26_on();
    }
    else if (command == "37"){
      Serial.println("37");
      led37_on();
    }
    else if (command == "48"){
      Serial.println("48");
      led48_on();
    }
    else if (command == "12"){
      Serial.println("12");
      led12_on();
    }
    else if (command == "13"){
      Serial.println("13");
      led13_on();
    }
    else if (command == "14"){
      Serial.println("14");
      led14_on();
    }
    else if (command == "16"){
      Serial.println("16");
      l16 = true;
    }
    else if (command == "17"){
      Serial.println("17");
      l17 = true;
    }
    else if (command == "18"){
      Serial.println("18");
      l18 = true;
    }
    else if (command == "123"){
      Serial.println("123");
      led123_on();
    }
    else if (command == "124"){
      Serial.println("124");
      led124_on();
    }
    else if (command == "125"){
      Serial.println("125");
      l125 = true;
    }
    else if (command == "126"){
      Serial.println("126");
      l126 = true;
    }
    else if (command == "127"){
      Serial.println("127");
      l127 = true;
    }
    else if (command == "128"){
      Serial.println("128");
      l128 = true;
    }
    else if (command == "134"){
      Serial.println("134");
      led134_on();
    }
    else if (command == "135"){
      Serial.println("135");
      l135 = true;
    }
    else if (command == "136"){
      Serial.println("136");
      l136 = true;
    }
    else if (command == "137"){
      Serial.println("137");
      l137 = true;
    }
    else if (command == "138"){
      Serial.println("138");
      l138 = true;
    }
    else if (command == "145"){
      Serial.println("145");
      l145 = true;
    }
    else if (command == "146"){
      Serial.println("146");
      l146 = true;
    }
    else if (command == "147"){
      Serial.println("147");
      l147 = true;
    }
    else if (command == "148"){
      Serial.println("148");
      l148 = true;
    }
    else if (command == "156"){
      Serial.println("156");
      led156_on();
    }
    else if (command == "157"){
      Serial.println("157");
      led157_on();
    }
    else if (command == "158"){
      Serial.println("158");
      led158_on();
    }
    else if (command == "167"){
      Serial.println("167");
      l167 = true;
    }
    else if (command == "168"){
      Serial.println("168");
      l168 = true;
    }
    else if (command == "178"){
      Serial.println("178");
      l178 = true;
    }
    else if (command == "1234"){
      Serial.println("1234");
      led1234_on();
    }
    else if (command == "1256"){
      Serial.println("1256");
      led1256_on();
    }
    else if (command == "1357"){
      Serial.println("1357");
      led1357_on();
    }
    else if (command == "1458"){
      Serial.println("1458");
      led1458_on();
    }
    else if (command == "cl"){
      Serial.println("cl");
      circle_of_leds();
    }
    else if (command == "cp"){
      Serial.println("cp");
      circle_of_pairs();
    }
    else if (command == "cp1"){
      Serial.println("cp1");
      circle_of_pairs_led1();
    }
    else if (command == "ct1"){
      Serial.println("ct1");
      circle_of_trios_led1();
    }
  }
  
  if (l16) {
    led16();
  }
  else if (l17){
    led17();
  }
  else if (l18){
    led18();
  }
  else if (l125){
    led125();
  }
  else if (l126){
    led126();
  }
  else if (l127){
    led127();
  }
  else if (l128){
    led128();
  }
  else if (l135){
    led135();
  }
  else if (l136){
    led136();
  }
  else if (l137){
    led137();
  }
  else if (l138){
    led138();
  }
  else if (l145){
    led145();
  }
  else if (l146){
    led146();
  }
  else if (l147){
    led147();
  }
  else if (l148){
    led148();
  }
  else if (l167){
    led167();
  }
  else if (l168){
    led168();
  }
  else if (l178){
    led178();
  }
}

void turn_off(){
    digitalWrite(minpin1, HIGH);
    digitalWrite(minpin2, HIGH);
    digitalWrite(ledpin1, LOW);
    digitalWrite(ledpin2, LOW);
    digitalWrite(ledpin3, LOW);
    digitalWrite(ledpin4, LOW);
  }


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//ДАЛЬШЕ
//ИДУТ
//ВКЛЮЧЕНИЯ ПО 1
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void circle_of_leds(){
  led1_on();
  delay(500);
  led1_off();
  led2_on();
  delay(500);
  led2_off();
  led3_on();
  delay(500);
  led3_off();
  led4_on();
  delay(500);
  led4_off();
  led5_on();
  delay(500);
  led5_off();
  led6_on();
  delay(500);
  led6_off();
  led7_on();
  delay(500);
  led7_off();
  led8_on();
  delay(500);
  led8_off();
  }
  
void led1_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin1, HIGH);
  }
void led1_off(){
  digitalWrite(ledpin1, LOW);
  digitalWrite(minpin1, HIGH);
  }

void led2_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin2, HIGH);
  }
void led2_off(){
  digitalWrite(ledpin2, LOW);
  digitalWrite(minpin1, HIGH);
  }

void led3_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin3, HIGH);
  }
void led3_off(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin3, LOW);
  }

void led4_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin4, HIGH);
  }
void led4_off(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin4, LOW);
  }

void led5_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin1, HIGH);
  }
void led5_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin1, LOW);
  }
  
void led6_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin2, HIGH);
  }
void led6_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin2, LOW);
  }

void led7_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin3, HIGH);
  }
void led7_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin3, LOW);
  }

void led8_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin4, HIGH);
  }
void led8_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin4, LOW);
  }


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//ДАЛЬШЕ
//ИДУТ
//ВКЛЮЧЕНИЯ ПО 2
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void circle_of_pairs(){
  led15_on();
  delay(500);
  led15_off();
  led26_on();
  delay(500);
  led26_off();
  led37_on();
  delay(500);
  led37_off();
  led48_on();
  delay(500);
  led48_off();
  }

void led12_on(){
    led1_on();
    led2_on();
  }

void led12_off(){
    led1_off();
    led2_off();
  }

void led13_on(){
    led1_on();
    led3_on();
  }

void led13_off(){
    led1_off();
    led3_off();
  }

void led14_on(){
    led1_on();
    led4_on();
  }

void led14_off(){
    led1_off();
    led4_off();
  }

void led15_on(){
    led1_on();
    led5_on();
  }

void led15_off(){
    led1_off();
    led5_off();
  }

void led26_on(){
    led2_on();
    led6_on();
  }

void led26_off(){
    led2_off();
    led6_off();
  }
  
void led37_on(){
    led3_on();
    led7_on();
  }

void led37_off(){
    led3_off();
    led7_off();
  }

void led48_on(){
    led4_on();
    led8_on();
  }
void led48_off(){
    led4_off();
    led8_off();
  }

void circle_of_pairs_led1(){
  led12_on();
  delay(500);
  led12_off();
  led13_on();
  delay(500);
  led13_off();
  led14_on();
  delay(500);
  led14_off();
  led15_on();
  delay(500);
  led15_off();
  unsigned long millis_start = millis();
  while (millis()-millis_start<500) {
      led16();
    }
  while (millis()-millis_start<1000) {
      led17();
    }
  while (millis()-millis_start<1500) {
      led18();
    }
  }

void led16(){
    led1_on();
    led1_off();
    led6_on();
    led6_off();
  }

void led17(){
    led1_on();
    led1_off();
    led7_on();
    led7_off();
  }

void led18(){
    led1_on();
    led1_off();
    led8_on();
    led8_off();
  }


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//ДАЛЬШЕ
//ИДУТ
//ВКЛЮЧЕНИЯ ПО 3
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void circle_of_trios_led1(){
  int timer = 2000;
  led123_on();
  delay(timer);
  led123_off();
  led124_on();
  delay(timer);
  led124_off();
  unsigned long millis_start = millis();
  while (millis()-millis_start<timer*1) {
      led125();
    }
  while (millis()-millis_start<timer*2) {
      led126();
    }
  while (millis()-millis_start<timer*3) {
      led127();
    }
  while (millis()-millis_start<timer*4) {
      led128();
    }
  led134_on();
  delay(timer);
  led134_off();
  while (millis()-millis_start<timer*6) {
      led135();
    }
  while (millis()-millis_start<timer*7) {
      led136();
    }
  while (millis()-millis_start<timer*8) {
      led137();
    }
  while (millis()-millis_start<timer*9) {
      led138();
    }
  while (millis()-millis_start<timer*10) {
      led145();
    }
  while (millis()-millis_start<timer*11) {
      led146();
    }
  while (millis()-millis_start<timer*12) {
      led147();
    }
  while (millis()-millis_start<timer*13) {
      led148();
    }
  led156_on();
  delay(timer);
  led156_off();
  led157_on();
  delay(timer);
  led157_off();
  led158_on();
  delay(timer);
  led158_off();
  while (millis()-millis_start<timer*17) {
      led167();
    }
  while (millis()-millis_start<timer*18) {
      led168();
    }
  while (millis()-millis_start<timer*19) {
      led178();
    }
  }
  
void led123_on(){
    led12_on();
    led3_on();
  }
void led123_off(){
    led12_off();
    led3_off();
  }

void led124_on(){
    led12_on();
    led4_on();
  }
void led124_off(){
    led12_off();
    led4_off();
  }

void led125(){
    led12_on();
    led12_off();
    led5_on();
    led5_off();
  }

void led126(){
    led12_on();
    led12_off();
    led6_on();
    led6_off();
  }

void led127(){
    led12_on();
    led12_off();
    led7_on();
    led7_off();
  }

void led128(){
    led12_on();
    led12_off();
    led8_on();
    led8_off();
  }

void led134_on(){
    led13_on();
    led4_on();
  }
void led134_off(){
    led13_off();
    led4_off();
  }

void led135(){
    led13_on();
    led13_off();
    led5_on();
    led5_off();
  }

void led136(){
    led13_on();
    led13_off();
    led6_on();
    led6_off();
  }

void led137(){
    led13_on();
    led13_off();
    led7_on();
    led7_off();
  }

void led138(){
    led13_on();
    led13_off();
    led8_on();
    led8_off();
  }

void led145(){
    led14_on();
    led14_off();
    led5_on();
    led5_off();
  }

void led146(){
    led14_on();
    led14_off();
    led6_on();
    led6_off();
  }

void led147(){
    led14_on();
    led14_off();
    led7_on();
    led7_off();
  }

void led148(){
    led14_on();
    led14_off();
    led8_on();
    led8_off();
  }

void led156_on(){
    led15_on();
    led6_on();
  }
void led156_off(){
    led15_off();
    led6_off();
  }

void led157_on(){
    led15_on();
    led7_on();
  }
void led157_off(){
    led15_off();
    led7_off();
  }

void led158_on(){
    led15_on();
    led8_on();
  }
void led158_off(){
    led15_off();
    led8_off();
  }

void led167(){
    led1_on();
    led1_off();
    led6_on();
    led6_off();
    led7_on();
    led7_off();
  }

void led168(){
    led1_on();
    led1_off();
    led6_on();
    led6_off();
    led8_on();
    led8_off();
  }

void led178(){
    led1_on();
    led1_off();
    led7_on();
    led7_off();
    led8_on();
    led8_off();
  }


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//ДАЛЬШЕ
//ИДУТ
//ВКЛЮЧЕНИЯ ПО 4
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void led1234_on(){
    led12_on();
    led3_on();
    led4_on();
  }
void led1234_off(){
    led12_off();
    led3_off();
    led4_off();
  }

void led1256_on(){
    led1_on();
    led6_on();
  }

void led1256_off(){
    led1_off();
    led6_off();
  }

void led1357_on(){
    led1_on();
    led7_on();
  }

void led1357_off(){
    led1_off();
    led7_off();
  }

void led1458_on(){
    led1_on();
    led8_on();
  }

void led1458_off(){
    led1_off();
    led8_off();
  }
