#define PIN_TRIG 12
#define PIN_ECHO 11
long duration, cm;
bool echo_mode = false;

#define SENS_PIN A0
float voltage; // переменная для хранения значения АЦП
float dist; // переменная для хранения вычисленного расстояния
bool sharp_mode = false;

void setup() {
  // Инициализируем взаимодействие по последовательному порту
  Serial.begin (9600);
  //Определяем вводы и выводы
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  pinMode(SENS_PIN, INPUT);
}

void echo() {
  digitalWrite(PIN_TRIG, LOW);
  delayMicroseconds(5);
  digitalWrite(PIN_TRIG, HIGH);
  // Выставив высокий уровень сигнала, ждем около 10 микросекунд. В этот момент датчик будет посылать сигналы с частотой 40 КГц.
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);
  //  Время задержки акустического сигнала на эхолокаторе.
  duration = pulseIn(PIN_ECHO, HIGH);
  // Теперь осталось преобразовать время в расстояние
  cm = (duration / 2) / 29.1;
  //cm = map(cm, 0, 2000, 0, 80);
  Serial.print("Echo distance: ");
  Serial.print(cm);
  Serial.println(" cm.");
}

void sharp() {
  float a = analogRead(SENS_PIN) * 5.0 / 1024.0;
  dist = 24.0 * pow(a, -1); // вычисляем расстояние
  //выводим показания в монитор порта
  Serial.print("Inf distance: ");
  Serial.print(dist);
  Serial.println(" cm");
}

void loop() {
  // Сначала генерируем короткий импульс длительностью 2-5 микросекунд.
  char inp = Serial.read();
  if (inp == 'e') {
    echo_mode = !echo_mode;
  } else if (inp == 'i') {
    sharp_mode = !sharp_mode;
  }
  if(echo_mode){
    echo();
  }
  if(sharp_mode){
    sharp();
  }
  // Задержка между измерениями для корректной работы скеча
  delay(1000);
}