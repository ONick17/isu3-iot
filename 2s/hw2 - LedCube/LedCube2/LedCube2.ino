#define ledpin1 2
#define ledpin2 3
#define ledpin3 4
#define ledpin4 5
#define minpin1 6
#define minpin2 7
int leds[8] = {1, 2, 3, 4, 5, 6, 7, 8};
int cnt;
float timer;
char chr;


void setup (){
  Serial.begin(9600);
  pinMode(ledpin1, OUTPUT);
  pinMode(ledpin2, OUTPUT);
  pinMode(ledpin3, OUTPUT);
  pinMode(ledpin4, OUTPUT);
  pinMode(minpin1, OUTPUT);
  pinMode(minpin2, OUTPUT);
  digitalWrite(ledpin1, LOW);
  digitalWrite(ledpin2, LOW);
  digitalWrite(ledpin3, LOW);
  digitalWrite(ledpin4, LOW);
  digitalWrite(minpin1, HIGH);
  digitalWrite(minpin2, HIGH);
}


void led1_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin1, HIGH);
  }
void led1_off(){
  digitalWrite(ledpin1, LOW);
  digitalWrite(minpin1, HIGH);
  }

void led2_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin2, HIGH);
  }
void led2_off(){
  digitalWrite(ledpin2, LOW);
  digitalWrite(minpin1, HIGH);
  }

void led3_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin3, HIGH);
  }
void led3_off(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin3, LOW);
  }

void led4_on(){
  digitalWrite(minpin1, LOW);
  digitalWrite(ledpin4, HIGH);
  }
void led4_off(){
  digitalWrite(minpin1, HIGH);
  digitalWrite(ledpin4, LOW);
  }

void led5_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin1, HIGH);
  }
void led5_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin1, LOW);
  }
  
void led6_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin2, HIGH);
  }
void led6_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin2, LOW);
  }

void led7_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin3, HIGH);
  }
void led7_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin3, LOW);
  }

void led8_on(){
  digitalWrite(minpin2, LOW);
  digitalWrite(ledpin4, HIGH);
  }
void led8_off(){
  digitalWrite(minpin2, HIGH);
  digitalWrite(ledpin4, LOW);
  }


void (*lenX_on[])() = {
  led1_on,
  led2_on,
  led3_on,
  led4_on,
  led5_on,
  led6_on,
  led7_on,
  led8_on
};
void (*lenX_off[])() = {
  led1_off,
  led2_off,
  led3_off,
  led4_off,
  led5_off,
  led6_off,
  led7_off,
  led8_off
};


void loop(){
  if (Serial.available() > 0){
    delay(1);
    cnt = 0;
    String command = Serial.readString();
    command.trim();
    //Serial.println(command);
    if (command == "cl"){
      Serial.println("cl");
      circle_of_leds();
    } else {
      for (byte i = 0; i <= command.length(); i++) {
        chr = command[i];
        if ((isDigit(chr)) and (cnt < 8)) {
          if (((chr - '0') > 0) and ((chr - '0') < 9)){
            leds[cnt] = (chr - '0');
            Serial.print(chr);
            cnt += 1;
          }
        }
      }
      timer = 1/cnt;
      Serial.println();
    }
  }
  for (byte i = 0; i < cnt; i++){
      lenX_on[leds[i]-1]();
      delay(timer);
      lenX_off[leds[i]-1]();
    }
}


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//ДАЛЬШЕ
//ИДУТ
//ВКЛЮЧЕНИЯ ПО 1
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void circle_of_leds(){
  led1_on();
  delay(500);
  led1_off();
  led2_on();
  delay(500);
  led2_off();
  led3_on();
  delay(500);
  led3_off();
  led4_on();
  delay(500);
  led4_off();
  led5_on();
  delay(500);
  led5_off();
  led6_on();
  delay(500);
  led6_off();
  led7_on();
  delay(500);
  led7_off();
  led8_on();
  delay(500);
  led8_off();
}
