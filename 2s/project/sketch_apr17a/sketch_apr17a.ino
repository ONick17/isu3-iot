#include "FastLED.h"
//#include "WiFi.h"
#include <ESP8266WiFi.h>
#include <espnow.h>
#define APIN_PHOTO A0
bool led_on = false;
const int DPIN_LED = 4;
const int DPIN_MOVE = 5;
const int led_num = 10;
CRGB leds[led_num];

uint8_t mac_address0[] = {0x48, 0x55, 0x19, 0xC8, 0x74, 0x19};
uint8_t mac_address1[] = {0x48, 0x55, 0x19, 0xC1, 0xB4, 0x0E};
uint8_t mac_address2[] = {0x48, 0x55, 0x19, 0xC8, 0x4B, 0x43};
uint8_t mac_address3[] = {0x40, 0x91, 0x51, 0x55, 0xC0, 0xBE};

const int esp_num = 0;

int incoming_data = 0;
bool incoming_check = false;

void onIncoming(uint8_t * mac, uint8_t *incomingData, uint8_t len) { 
  memcpy(&incoming_data, incomingData, sizeof(incoming_data));
  incoming_check = true;
}

void setup() {
  Serial.begin(9600);
  //WiFi.mode(WIFI_AP_STA);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  if (esp_num == 0) {
    esp_now_add_peer(mac_address1, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  } else if (esp_num == 1) {
    esp_now_add_peer(mac_address0, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
    esp_now_add_peer(mac_address2, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  } else if (esp_num == 2) {
    esp_now_add_peer(mac_address1, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
    esp_now_add_peer(mac_address3, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  } else if (esp_num == 3) {
    esp_now_add_peer(mac_address2, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
  }
  esp_now_register_recv_cb(onIncoming);
  
  pinMode(APIN_PHOTO, INPUT);
  pinMode(DPIN_MOVE, INPUT);
  pinMode(DPIN_LED, OUTPUT);
  FastLED.addLeds<WS2812, DPIN_LED, GRB>(leds, led_num);
  Serial.print("mac-адрес: ");
  Serial.println(WiFi.macAddress());
}


int brightness;
int brightness_start;
int speed_ = 20;
int delay_ = 0;
int start_ = 4;

void turnLed(bool mode_) {
  if (mode_) {
    if (led_on == false) {
      brightness_start = 0;
      for (int i=0; i<5; i++){
        brightness = brightness_start;
        while (brightness <= 255) {
          if (i == 0) {
            leds[start_] = CRGB(brightness, brightness, brightness);
          } else {
            leds[start_ + i] = CRGB(brightness, brightness, brightness);
            leds[start_ - i] = CRGB(brightness, brightness, brightness);
          }
          FastLED.show();
          delay(delay_);
          brightness += speed_;
        }
      }
      //FastLED.showColor(CRGB::White);
      //FastLED.setBrightness(255);
      led_on = true;
    }
  } else {
    if (led_on == true) {
      brightness_start = 255;
      for (int i=4; i>-1; i--){
        brightness = brightness_start;
        while (brightness >= 0) {
          if (i == 0) {
            leds[start_] = CRGB(brightness, brightness, brightness);
          } else {
            leds[start_ + i] = CRGB(brightness, brightness, brightness);
            leds[start_ - i] = CRGB(brightness, brightness, brightness);
          }
          FastLED.show();
          delay(delay_);
          brightness -= speed_;
        }
      }
      FastLED.clear();
      FastLED.show();
      led_on = false;
    }
  }
}

int tm_start = 0;
int tm_wait = 5000;
int tries_to_sleep = 3;
int try_ = 0;

void loop_main(){
  if (millis()-tm_start > tm_wait) {
    turnLed(false);
  }
  int pht = analogRead(APIN_PHOTO);
  //if (pht > 500) {
  if (true) {
    try_ = 0;
    if (incoming_check){
      tm_start = millis();
      turnLed(true);
      incoming_check = false;
    }
    int mv = digitalRead(DPIN_MOVE);
    Serial.print("Фоторезистор: ");
    Serial.print(pht);
    Serial.print(". Движение: ");
    Serial.println(mv);
    if (mv == HIGH) {
      if (esp_num == 0) {
        esp_now_send(mac_address1, (uint8_t *) &mv, sizeof(mv));
      } else if (esp_num == 1) {
        esp_now_send(mac_address0, (uint8_t *) &mv, sizeof(mv));
        esp_now_send(mac_address2, (uint8_t *) &mv, sizeof(mv));
      } else if (esp_num == 2) {
        esp_now_send(mac_address1, (uint8_t *) &mv, sizeof(mv));
        esp_now_send(mac_address3, (uint8_t *) &mv, sizeof(mv));
      } else if (esp_num == 3) {
        esp_now_send(mac_address2, (uint8_t *) &mv, sizeof(mv));
      }
      tm_start = millis();
      turnLed(true);
    }
  } else {
    Serial.print("Фоторезистор: ");
    Serial.println(pht);
    if ((try_ >= tries_to_sleep) and not led_on) {
      Serial.println("Ухожу в сон");
      //ESP.deepSleep(300e6);
    } else {
      try_++;
    }
  }
}

void loop_mac() {
  Serial.print("mac-адрес: ");
  Serial.println(WiFi.macAddress());
}

void loop_concheck() {
  if (millis()-tm_start > tm_wait) {
    if (led_on == true) {
      FastLED.clear();
      FastLED.show();
      led_on = false;
      Serial.println("off");
    }
  }
  if (incoming_check){
    tm_start = millis();
    Serial.println("update");
    if (led_on == false) {
      led_on = true;
      Serial.println("on");
    }
    incoming_check = false;
  }
}

void loop_nophoto(){
  if (millis()-tm_start > tm_wait) {
    turnLed(false);
    analogWrite(DPIN_LED, LOW);
  }
  if (incoming_check){
    tm_start = millis();
    analogWrite(DPIN_LED, HIGH);
    incoming_check = false;
  }
  int mv = digitalRead(DPIN_MOVE);
  Serial.print(". Движение: ");
  Serial.println(mv);
  if (mv == HIGH) {
    if (esp_num == 0) {
      esp_now_send(mac_address1, (uint8_t *) &mv, sizeof(mv));
    } else if (esp_num == 1) {
      esp_now_send(mac_address0, (uint8_t *) &mv, sizeof(mv));
      esp_now_send(mac_address2, (uint8_t *) &mv, sizeof(mv));
    } else if (esp_num == 2) {
      esp_now_send(mac_address1, (uint8_t *) &mv, sizeof(mv));
      esp_now_send(mac_address3, (uint8_t *) &mv, sizeof(mv));
    } else if (esp_num == 3) {
      esp_now_send(mac_address2, (uint8_t *) &mv, sizeof(mv));
    }
    tm_start = millis();
    analogWrite(DPIN_LED, HIGH);
  } else {
    analogWrite(DPIN_LED, LOW);
  }
}

void loop_nomove(){
  if (incoming_check){
    analogWrite(DPIN_LED, LOW);
    Serial.println(millis());
    incoming_check = false;
  } else {
    analogWrite(DPIN_LED, HIGH);
  }
}

void loop() {
  //loop_main();
  loop_mac();
  //loop_concheck();
  //loop_nophoto();
  //loop_nomove();
}
