#include <SoftwareSerial.h>
#define pin_speaker 2
#define pin_mode 7
#define pin_led 4
#define pin_display_main 3
#define pin_display_move 5
#define pin_display_exit 6
#define apin_level A0
//SoftwareSerial mySerial(11, 10);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {}
  //mySerial.begin(9600);
  //while (!mySerial) {}

  pinMode(pin_led, OUTPUT);
  pinMode(pin_speaker, OUTPUT);
  pinMode(pin_display_main, OUTPUT);
  pinMode(pin_display_move, OUTPUT);
  pinMode(pin_display_exit, OUTPUT);
  
  pinMode(apin_level, INPUT);
  pinMode(pin_mode, INPUT);
}

void loop() {
  digitalWrite(pin_led, HIGH);
  delay(1000);
  digitalWrite(pin_led, LOW);
  delay(1000);
  int mode = digitalRead(pin_mode);
  int level = analogRead(apin_level);
  Serial.print(mode);
  Serial.print(' ');
  Serial.println(level);
}


//client
#define DATA_PIN 9
#define DATA_LEVEL LOW
String ENCODED[] = {".-", "--.."};
char LETTERS[] = {'A', 'Z'};
int n_letters = 2;
int TU = 1000;
void setup() {
  Serial.begin(9600);
  pinMode(DATA_PIN, OUTPUT);
  digitalWrite(DATA_PIN, !DATA_LEVEL);

}

void loop() {
  //TODO words
  if(Serial.available()>=1){
    char letter = Serial.read();
    for(int iletter=0; iletter< n_letters; iletter++){
      if(letter == LETTERS[iletter]){
        encode(ENCODED[iletter]);
      }
    }
  }

}

void encode(String dashdot){
  int len = dashdot.length();
  for (int isymbol=0; isymbol < len; isymbol++){
    if(dashdot[isymbol] == '.'){
      digitalWrite(DATA_PIN, DATA_LEVEL);
      delay(TU);
    }else{
      digitalWrite(DATA_PIN, DATA_LEVEL);
      delay(3*TU);
    }
    digitalWrite(DATA_PIN, !DATA_LEVEL);
      delay(TU);
  }
  delay(2*TU);
}


//server
#define DATA_PIN 2
#define LETTER_SEP 3
#define IDLE_TIME 10
#define DATA_LEVEL LOW
long start = 0;
int TU = 1000;
int timings[20];
bool levels[20];
bool check_letter = false;
bool is_idle = false;
int current = 0;
char dots_dash[20];
String ENCODED[] = {".-", "--.."};
String LETTERS[] = {"A", "Z"};
int n_letters = 2;

void setup() {
  Serial.begin(9600);
  pinMode(2, INPUT);
  attachInterrupt(0,process_timing, CHANGE ); // 0 -> digitalPin 2
}

void loop() {
  //TODO words separetions
  //TODO last letter before IDLE processing > 7 TU
  process_idle();
  if(check_letter){
    decode_letter();  
    check_letter = false;
  }
}

void decode_letter(){
  if(timings[current-1] >= LETTER_SEP and levels[current-1] == !DATA_LEVEL){
    String letter ="";
    for(int i =0; i<current; i++){
      if(timings[i] == 1 and levels[i] == DATA_LEVEL) {
        letter += '.';
      }else if(timings[i] == 3 and levels[i] == DATA_LEVEL){
        letter += '-';
      }
    }
    current = 0;
    for (int iletter=0; iletter<n_letters; iletter++){
      if(ENCODED[iletter] == letter){
        Serial.println(LETTERS[iletter]);
      }
    }
  }
}

void process_timing(){
  if (not is_idle){
  timings[current] = round((millis()-start + TU) /TU);
  levels[current] = !digitalRead(DATA_PIN);
  current++;
  check_letter = true;
  }
  start = millis();
  is_idle = false; 
}

void process_idle(){
  if (not is_idle and round((millis()-start) /TU)>= IDLE_TIME){
  timings[current] = 7;
  levels[current] = 1;
  current++;
  start = millis();
  is_idle = true;
  }
}

