import time
import paho.mqtt.client as mqtt_client
import random

broker = "broker.emqx.io"
client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}')

try:
    client.connect(broker)
except Exception:
    print("Failed to connect.")
    exit()

chanel = 'lab/ls/Bladik'
mes = ''
while mes != "exit":
    mes = input()
    client.publish(chanel, mes)
    if (mes.find("!FLOOD!") != -1):
        times = int(mes[mes.find("!FLOOD!")+7:mes.find("!FLOOD!")+10])
        mes = mes[mes.find("!FLOOD!")+10:]
        for i in range(times):
            client.publish(chanel, mes)
            time.sleep(0.5)
