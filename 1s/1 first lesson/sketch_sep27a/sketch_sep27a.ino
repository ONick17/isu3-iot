#define led_pin 13
#define sensor_pin 40

void setup(){
  pinMode(led_pin, OUTPUT);
  Serial.begin(9600);
  
}

void loop(){
  digitalWrite(led_pin, HIGH);
  delay(50);
  Serial.println("Led is on");
  digitalWrite(led_pin, LOW);
  delay(50);
  Serial.println("Led is off");

  int val =  analogRead(sensor_pin);
  Serial.println(val);
  delay(10);
}