import serial
from serial.tools import list_ports
import time
import sys

port = "COM12"

def hash_find2(txt):
    txt_arr = []
    for i in range(len(txt)):
        txt_arr.append(ord(txt[i]))
    hsh = ""
    ln = 6
    for i in range(7):
        num = 0
        for j in txt_arr:
            num += (j >> (ln - i)) % 2
        num %= 2
        hsh += str(num)
    return(hsh)

def check_connect(x):
    for i in list_ports.comports():
        if (i.device == x):
            return True
    return False

def get_signal(ser, pin=None):
    tm = 0
    while ser.in_waiting == 0:
        time.sleep(0.5)
        tm += 1
        if tm == 6:
            return
    if ser.in_waiting > 0:
        time.sleep(1)
        data = ser.readline(ser.in_waiting)
        print(data.decode())

def print_help():
    print()
    print("!!!!!!!!!!!!!!!!!!!!!!")
    print("help - list of commands")
    print("close - close programm")
    print("stream - read all singals from MCU")
    print("single - read one singal from MCU")
    print("apin read X - read value of analog pin X on MCU")
    print("apins read - read values of analog pins 0-5 on MCU")
    print("dpin read X - read value of digital pin X on MCU")
    print("dpins read - read values of digital pins 0-13 on MCU")
    print("dpin write X Y - change value of digital pin X to Y on MCU")
    print("dpins write Y - change values of digital pins 2-12 to Y on MCU")
    print("dpins write X1, ..., Xn to Y1, ..., Yn - change values of digital pins X1, ..., Xn to Y1, ..., Yn on MCU")
    print("!!!!!!!!!!!!!!!!!!!!!!")
    print()

if __name__ == "__main__":
    try:
        ser = serial.Serial(port, 9600)      
    except serial.SerialException:
        print("Error. Port", port, "has no devices.")
        sys.exit()
    apins = list(range(6))
    dpins = list(range(2, 14))
    print("Successful connection!")
    print("Type 'help' to see list of commands.")
    while True:
        mode = input("command: ").lower()

        
        #проверка наличия подключения
        if not check_connect(port):
            print("Connection is lost.")
            sys.exit()
        
        
        #выход из программы
        elif (mode == "close"):
            ser.close()
            sys.exit()

        #поточное чтение
        elif (mode == "stream"):
            print("Press ctrl+c to stop.")
            try:
                while True:
                    get_signal(ser)
            except KeyboardInterrupt:
                continue

        #разовое чтение
        elif (mode == "single"):
            print("Press ctrl+c to cansel.")
            try:
                get_signal(ser)
            except KeyboardInterrupt:
                continue

        #чтение значения аналагового пина
        #код 1
        elif (mode.find("apin read ") != -1):
            pin = mode.find("apin read ")+10
            try:
                pin = int(mode[pin:])
            except ValueError:
                print("Error. Check your input.")
                continue
            if pin in apins:
                ser.write("1".encode())
                time.sleep(1)
                ser.write(str(pin).encode())
                get_signal(ser)
            else:
                print("Error. Only 0-5 pins can be read.")

        #чтение значений аналаговых пинов
        #код 2
        elif (mode == "apins read"):
            ser.write("2".encode())
            for i in range(6):
                print("pin A", i, ": ", end="", sep="")
                get_signal(ser)

        #чтение значения цифрового пина
        #код 3
        elif (mode.find("dpin read ") != -1):
            pin = mode.find("dpin read ")+10
            try:
                pin = int(mode[pin:])
            except ValueError:
                print("Error. Check your input.")
                continue
            if pin in dpins:
                ser.write("3".encode())
                time.sleep(1)
                ser.write(str(pin).encode())
                get_signal(ser)
            else:
                print("Error. Only 2-13 pins can be read.")

        #чтение значений цифровых пинов
        #код 4
        elif (mode == "dpins read"):
            ser.write("4".encode())
            for i in range(14):
                print("pin D", i, ": ", end="", sep="")
                get_signal(ser)
            
        #изменение значения цифрового пина
        #код 5
        elif (mode.find("dpin write ") != -1):
            mode = mode[mode.find("dpin write ")+11:]
            data = mode.find(' ')+1
            if (data == 0):
                print("Error. Check your input.")
            try:
                pin = int(mode[:data])
                data = int(mode[data:])
            except ValueError:
                print("Error. Check your input.")
                continue
            if pin in dpins:
                if data in [0, 1]:
                    ser.write("5".encode())
                    time.sleep(1)
                    ser.write(str(pin).encode())
                    time.sleep(1)
                    ser.write(str(data).encode())
                else:
                    print("Error. Only 0 and 1 can be used as values.")
            else:
                print("Error. Only pins 2-13 can be read.")

        #dpins write X1, ..., Xn to Y1, ..., Yn
        #изменение значения цифровых пинов Xn на Yn
        #код 7
        elif (mode.find("dpins write") != -1) and (mode.find(" to ") != -1):
            pins = mode[mode.find("write ")+6:mode.find(" to ")]
            datas = mode[mode.find(" to ")+4:]
            try:
                pins = list(map(int, pins.split(', ')))
                datas = list(map(int, datas.split(', ')))
            except ValueError:
                print("Error. Check your input.")
                continue
            if len(pins) != len(datas):
                print("Error. Number of pins and number of values must be equal.")
            elif ([x for x in pins if x not in dpins]):
                print("Error. Only 2-13 pins can be changed.")
            elif ([x for x in datas if x not in [0, 1]]):
                print("Error. Only 0 and 1 can be used as values.")
            else:
                ser.write("7".encode())
                time.sleep(1)
                ser.write(str(len(pins)).encode())
                for i in range(len(pins)):
                    time.sleep(1)
                    ser.write(str(pins[i]).encode())
                    time.sleep(1)
                    ser.write(str(datas[i]).encode())
            
        #изменение значения цифровых пинов
        #код 6
        elif (mode.find("dpins write ") != -1):
            data = mode.find("dpins write ")+12
            try:
                data = int(mode[data:])
            except ValueError:
                print("Error. Check your input.")
                continue
            if data in [0, 1]:
                ser.write("6".encode())
                time.sleep(1)
                ser.write(str(data).encode())
            else:
                print("Error. Only 0 and 1 can be used as values.")

        #отправить 7 тестовых строк с MCU
        #код 8
        elif (mode == "test single"):
            ser.write("8".encode())

        #вкл/выкл тестовой отправки данных с MCU в буфер
        #код 9
        elif (mode == "test stream"):
            ser.write("9".encode())

        #вывод списка команд
        elif (mode == "help"):
            print_help()
        else:
            print("Error. Type 'help' to see list of commands.")
