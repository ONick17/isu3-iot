import machine as mn
import time

pin = mn.Pin(2, mn.Pin.OUT)

for i in range(3):
    pin.value(1)
    time.sleep(1)
    pin.value(0)
    time.sleep(1)
    pin.value(1)
    time.sleep(0.5)
    pin.value(0)
    time.sleep(0.5)
pin.value(1)