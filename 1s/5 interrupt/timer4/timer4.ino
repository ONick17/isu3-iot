#define a_pin A0
bool mode4 = false;
int chk = 0;

void setup(){
  Serial.begin(9600);
  pinMode(a_pin, INPUT);

  cli();
  //4
  TCCR1A = 0;
  TCCR1B = 0;
  OCR1A = 3900;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  TCCR1B |= (1 << CS12);
  TIMSK1 |= (1 << OCIE1A);
  sei();
}

//4
ISR(TIMER1_COMPA_vect){
  mode4 = true;
}

void loop(){
  //4
  if (mode4){
    mode4 = false;
    Serial.print(millis() - chk);
    chk = millis();
    Serial.print(' ');
    Serial.println(analogRead(a_pin));
  }
}