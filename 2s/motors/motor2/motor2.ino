#define motor_pin 10

void setup() {
  pinMode(motor_pin, OUTPUT);
}

void loop() {
  for(int i=0; i<=255; i++) {
    analogWrite(motor_pin, i);
    delay(20);
  }
  for(int i=255; i>=0; i--) {
    analogWrite(motor_pin, i);
    delay(20);
  }
}
